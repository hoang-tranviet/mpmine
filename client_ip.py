#! /usr/bin/env python

# INPUT:  sf_conn
# OUTPUT: 
# 	count distinctive client IPs  
#	count number of network prefixes for IPv4 and IPv6

import os
from IPy import IP
STATE = 0
IP_SRC = 3
IP_DST = 4

serverIP = set(["2001:6a8:3080:1:216:3eff:fec5:c815","130.104.230.45"])
clientIPs = set()

v4net=set()
v6net=set()


def process(f):
	s = f.split('_')
	conn_id = s[2]

	sfs = []	# list of subflows in a connection
	for l in open(f):
		sf = l.strip().split(',')
		sfs.append(sf)

	if len(sfs) == 0:
		return
	isf = sfs[len(sfs)-1]	# get the last element

	for sf in sfs:
		ip = (sf[IP_SRC])
		if ip not in clientIPs:
			clientIPs.add(ip)

	return 

def getPrefixStats(dir, outdir):
	ipv4 = 0
	ipv6 = 0
	of = open( outdir +'/ip-prefixes.txt','w+')
	of2 = open(outdir +'/ips-for-cymru-webservice.txt','w+')

	of2.write("begin\n")
	print os.getcwd()

	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			process(f)

	for ip in clientIPs:
		of2.write( ip + "\n")

		if IP(ip).version() == 4:
			ipv4 += 1
			# trim the right most 8 bits -> prefix /24
			(prefix, dot, suffix) = ip.rpartition('.')
			# trim the next 8 bits -> prefix /16
			(prefix, dot, suffix) = prefix.rpartition('.')
			v4net.add(prefix)
		elif IP(ip).version() ==6:
			ipv6 += 1
			#ip6 = IP(ip).make_net('ffff:ffff:ffff::')
			subnet = IP(ip+'/48', make_net=True)
			v6net.add(subnet)

	print "16-bit net prefix ipv4:", len(v4net), "IPv4 addresses:",ipv4
	print "48-bit net prefix ipv6:", len(v6net), "IPv6 addresses:",ipv6
	print "number of distinctive client IPs:", len(clientIPs)

	of.write( "IPv4 addresses:"+ str(ipv4) +"	16-bit net prefix ipv4:"+ str(len(v4net)) +"\n") 
	of.write( "IPv6 addresses:"+ str(ipv6) +"	48-bit net prefix ipv6:"+ str(len(v6net)) +"\n") 
	of.write( "number of distinctive client IPs:"+ str(len(clientIPs)) +"\n")

	of2.write("end")

# getPrefixStats(".", "out")

# below is the query command to the cymru.com webservice for the ASnumbers 
"""
 os.system("netcat whois.cymru.com 43 < out/ips-for-cymru-webservice.txt | sort -n > out/ips-ASNumbers.txt")
"""

ASNs = set()
asn_file = "out/ips-ASNumbers.txt"

for line in open(asn_file):
	l = line.split('|')
	if len(l)== 3:
		ASNumber = l[0].strip()
		if (ASNumber not in ASNs) and (ASNumber != "NA"):
			print ASNumber
			ASNs.add(ASNumber)

print "number of unique ASes that clients belong to: ", len(ASNs)