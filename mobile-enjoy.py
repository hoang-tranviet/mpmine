#! /usr/bin/env python

# INPUT: week/day
# OUTPUT: results from all traces in the week/day
# use mptcptrace as backend

from subprocess import call
from datetime import *

import time
import os
import argparse

parser = argparse.ArgumentParser(description="Wrapper for mptcptrace")
parser.add_argument('--dir', '-d',
                    help="Directory to store outputs",
					default="../../imc/real_smartphones")
					# captured from multipath-tcp.org

parser.add_argument('--all', '-a', default= False, action='store_true',
					dest="all")		# analyse ALL traces, warning: maybe too much


parser.add_argument('--plot', '-p',
					default=False, action='store_true',
					dest="plot")

# '-n': do not run mptcptrace (if it was used before) 
parser.add_argument('--notraceparsing', '-n', default= True,
					action='store_false', dest='parsing')
# extract desired connection to a pcap file?
parser.add_argument('--extract', '-e', default= False, action='store_true')


# Expt parameters
args = parser.parse_args()

indir = os.path.abspath(args.dir) 

# process all traces
outdir = indir +'/all/'

if not os.path.exists(outdir):
	os.makedirs(outdir)
os.chdir(outdir)
print os.getcwd()



# import globals
# tracelist = []

# create_symlink(dir):
for trace in sorted(os.listdir(indir)):
	# tracelist.append(str(trace))
	if os.path.islink(outdir+trace):	# skip if symlink is created already
		continue
	call('ln -s '+ indir+'/'+trace + '  '+ outdir+trace, shell=True)

# globals.tracelist = tracelist

if args.parsing:
	loglevel = ' -l 1 '
	cmd = "mptcptrace -d "+ outdir + " -t 1000 -v -S " + loglevel # + duration
	print cmd
	print ""
	# clean temp files:
	call("rm sf_conn_* conn_* stats*.csv", shell=True)
	return_code = call (cmd, shell=True)

import addrStats
#addrStats.getStats(dir, outdir)
import client_ip
#client_ip.getPrefixStats(dir, outdir)
import conTime
# conTime.main(outdir, False)

import nflows
#nflows.main(outdir,outdir)
import pathmanager
#pathmanager.main(outdir, outdir)
import extractData
#extractData.main(outdir, outdir)
import data_rtt_2sf
#data_rtt_2sf.main(outdir, outdir, args.extract, args.plot)
import data_2sf
#data_2sf.getData(outdir, outdir)

if args.plot:
	raw_input('Press any key to finish')
