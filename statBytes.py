#! /usr/bin/env python

# INPUT like this:
# mptcptrace/src$ cat sf_conn_49
# 2,0,0
# 2,1,1				// this sf should contain datafin
# 2,0,0
# 2,0,0
# 3,2026,3640			// last line is isf!

# OUTPUT:
# bytes over mpconn  -> cdf 
#		over sf  -> cdf ;	use tcptrace?

import os
from subprocess import call
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
mpData = []
sfData = []

def process(f, of2, of3):
	s = f.split('_')
	conn_id = s[2]
	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		if int(sf[STATE]) < 3:		# SYN =1, SYNACK = 2,  ACK = 3
			continue		# only consider conn with isf finish 3wHS
		sfs.append(sf)

	if len(sfs) == 0:
		return
	totalByte = 0
	for sf in sfs:
		sfByte = long(sf[BYTE_C2S]) + long(sf[BYTE_S2C])
		sfData.append(sfByte)
		of3.write(conn_id+'\t'+ str(sfByte) +'\n')
		totalByte += sfByte 
		
	mpData.append(totalByte)
	of2.write(str(conn_id) + '\t' +str(totalByte)+ '\n')
	#if totalByte<=2:
	#	os.system("cat "+ str(dir) +"conn_"+ conn_id +"|  grep isf > empty-conn.txt")

def main(dir, outdir ):
	print os.getcwd()
	print 'Total bytes of each connection'
	of2 = open(outdir +'mp-data.txt','w+')
	of3 = open(outdir +'sf-data.txt','w+')
	of2.write("# ConnID		mp_bytes\n")
	of3.write("# ConnID		sf_bytes\n")
	for f in os.listdir(dir):
		if f.startswith('sf_conn_'):
			process(f, of2, of3)

#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(sfData),len(mpData)

	fig, ax = plt.subplots()
	x=np.sort(sfData)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='subflow bytes')

	x=np.sort(mpData)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='mptcp bytes')
	
	ax.set_xscale('log')
	ax.legend( loc = 'lower right')
	plt.show(block=False)
	plt.savefig(outdir +'mpBytes.eps', bbox_inches='tight')

if __name__ == '__main__':
	dir = '.'
	main(dir, dir + '/out/')
