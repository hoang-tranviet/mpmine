#! /usr/bin/env python

# INPUT: delay-join.txt

# OUTPUT:
#	plot delay-join
#	query long delay

import os
import argparse
from subprocess import call
 
delay1 = []
delay = []

def getData(f):
	data = []
	for l in f:
		if l.startswith('#'):
			continue
		conn = l.strip().split('\t')

		if len(conn) > 1:
			data.append(float(conn[1]))

			if float(conn[1]) > 10000:
				print conn
	return data

def main(dir, outdir ):
	print os.getcwd()
	f1 = open(dir +'delay-1st-join.txt','r+')
	f2 = open(dir +'delay-join.txt','r+')

	delay1 = getData(f1)
	delay = getData(f2)


#plot
	import numpy as np
	import matplotlib.pyplot as plt

	print len(delay1),len(delay)

	fig, ax = plt.subplots()

	x=np.sort(delay1)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, '--', lw=1.5, color = 'blue', label='first join')
	
	x=np.sort(delay)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.5, color = 'red', label='all joins')
	
	ax.set_xscale('log')
	ax.legend(loc='center right')	
	plt.xlabel('Time of subflow joins from CAPA_SYN (s)')
	plt.ylabel('CDF')

	plt.grid()
	plt.savefig(outdir +'delay-join.pdf', bbox_inches='tight')
	plt.show()

if __name__ == '__main__':
	dir = './'
	main(dir, dir)
