#!/bin/bash

# This script download, install mptcptrace, and configure $PATH environment

# Install the requirement/dependent packages:
sudo apt-get install autoconf check libssl-dev  libpcap-dev

# clone the modified version of mptcptrace
if [ ! -d mptcptrace ]; then
	git clone https://hoang-tranviet@bitbucket.org/hoang-tranviet/mptcptrace.git
fi
cd mptcptrace
git fetch && git checkout imc

# autoconf and install mptcptrace
./autogen.sh
./configure 
make
sudo make install

# add path of mpmine to PATH environment variable
cd ..
export PATH=$PATH:`pwd`