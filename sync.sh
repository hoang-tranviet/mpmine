#!/bin/bash

cd ../../imc/mptcp167/
rsync -e "ssh -i ~/.ssh/id_rsa"  --ignore-existing  -ivhP hoang@nos:/home/mpcollect/collect/mptcp167/*.xz  .

# todo: if trace not exist -> unzip if did not -> copy trace
for zipfile in *.xz 
do
	trace=${zipfile%.tar.xz}
	echo $trace
	
	file=${trace}.pcap
	# copy trace from subfolder if it doesn't exist.
	if [ ! -f $file ]; then
		# if folder not exist, unzip
		if [ ! -d "$trace" ]; then 
			echo "folder not exist: ${trace}"
			tar --keep-old-files -xvf  ${zipfile}
		fi
		cd ${trace}
		editcap  -F libpcap  *.pcap  ../${trace}.pcap
		cd ..
		# remove folder
		rm -r $trace
	fi
done
