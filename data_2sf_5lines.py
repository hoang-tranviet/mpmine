#! /usr/bin/env python

# INPUT:
# sf_conn_*
# rtt-diff.txt: created by rtt-diff.py


# OUTPUT: (only consider mpconn having 2 sf)
#	fraction of data via isf over total,  -> cdf
#	bytes/packets over mpconn/isf  -> cdf ;	use tcptrace?

import os
from time import sleep
from subprocess import call
from IPy import IP
import pathmanager
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IP_SRC = 3
IP_DST = 4
PORT_S = 5
PORT_D = 6

SYN 	= 1
SYNACK 	= 2
ACK 	= 3

ISFRatio = []

def process(f, of):
	s = f.split('_')
	conn_id = s[2]

	sfs = []

	for l in open(f):
		sf = l.strip().split(',')
		if l[0] == '#':
			continue
		sfs.append(sf)

	isf = sfs[-1]	# get the last element

	# remove sf not finished 3wHS, except isf
	for sf in sfs:		
		if int(sf[STATE]) < ACK and sf != isf:
			sfs.remove(sf)

	if len(sfs) != 2:
		return

	sf1= sfs[0]
	sf2= sfs[1]
	sf1bytes = long(sf1[BYTE_C2S]) + long(sf1[BYTE_S2C])
	sf2bytes = long(sf2[BYTE_C2S]) + long(sf2[BYTE_S2C])
	totalByte = sf1bytes + sf2bytes

	isfRatio = float(sf2bytes) / totalByte
	# of.write(conn_id +'\t'+ str(isfRatio)+'\n')

	if totalByte <= 2000:
		ISFRatio[0].append(isfRatio)

	if (totalByte > 2000) and totalByte <= 50000:	# [2 KB , 50 KB]
		ISFRatio[1].append(isfRatio)

	if (totalByte > 50000) and totalByte <= 500000:	# [50 KB , 500 KB]
		ISFRatio[2].append(isfRatio)

	if (totalByte > 500000) and totalByte <= 5000000:	# [500 KB , 5 MB]
		ISFRatio[3].append(isfRatio)

	if totalByte > 5000000:	# > 5 MB
		ISFRatio[4].append(isfRatio)

def main(dir, outdir):
	print os.getcwd()

	of = open(outdir +'isf-data-2sf.txt','w+')
	of.write("# ConnID		%isf_bytes\n")

	for i in range(5):
		ISFRatio.append([])

	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			process(dir+f, of)

#plot
	import numpy as np
	import matplotlib.pyplot as plt

	for i in range(5):
		print len(ISFRatio[i])

	# fig, ax = plt.subplots()

	# x=np.sort(ISFRatio)
	# cdf=np.arange(len(x))/float(len(x))
	# ax.plot( x, cdf, color = 'red')
	
	# # ax.set_xscale('log')
	# plt.xlabel('Percentage of data sent via ISF over all data')
	# plt.ylabel('CDF')
	# plt.savefig(outdir +'isf-data-2sf.pdf', bbox_inches='tight')
	# plt.show()

	fig, ax = plt.subplots()

	x=np.sort(ISFRatio[0])
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.5,  color = 'red', label='< 1 KB')


	x=np.sort(ISFRatio[1])
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.5, label='[2 KB , 50 KB]')


	x=np.sort(ISFRatio[2])
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.5, label='[50 KB , 500 KB]')

	x=np.sort(ISFRatio[3])
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf,'--',lw=1.5, label='[500 KB , 5 MB]')

	x=np.sort(ISFRatio[4])
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf,'--',lw=1.5, color = 'blue', label='> 5 MB')

	plt.xlim([0,1])
	ax.legend(loc='best')
	# ax.set_xscale('log')
	plt.xlabel('Percentage of data sent via ISF over all data')
	plt.ylabel('CDF')

	plt.savefig(outdir +'isf-data-2sf-5lines.pdf', bbox_inches='tight')
	plt.show()


if __name__ == '__main__':
	dir = '../'
	main(dir,'./')
