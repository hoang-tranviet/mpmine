mpmine
======

This repo includes scripts to analyse MPTCP traffic and to plot graphs from pcap files.

Requirements
-------------

 * mptcptrace (Benjamin Hesmans):
	However, these scripts use a modified mptcptrace to extract basic mptcp data.
	(https://bitbucket.org/hoang-tranviet/mptcptrace/src/b3619cefb993e5d0f147c196edef1b8edc8f7c35?at=imc)
 * tcptrace:
 	To get RTT of TCP connections. On Ubuntu, you can do "sudo apt-get install tcptrace"

Initialization:
---------------
For the first time usage, 

This script does two things:
* Download my modified version of mptcptrace, then compile and install it.
* Add location of mpmine into PATH environment variable.

However, mptcptrace may fail to install due to its dependency. In this case, we need to install it manually.

Usage
-----

1) First, run mptcptrace for the MPTCP trace. You can run on a single file, or on a folder of multiple files (scripts will sorted them by alphabetical order).

```bash
$	cd mptcp167
$	mptcptrace -d .  -t 10000 -v -S -l 1
```

2) Add path, so that you call scripts from everywhere:

```bash
$ 	export PATH=$PATH:/home/yourname/mpmine
```

3) Now you can run any desired script to get result and plot:

```bash
$ addrStats.py .

$ delayJoin.py .
$ cd out
$ plot-delayJoin.py
```

Additional scripts
------------------

isolate.sh: to isolate a connection in a large trace, give it the connection id, the script will use the filter in stats_{id}.csv to extract the connection.