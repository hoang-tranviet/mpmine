#! /usr/bin/env python

# INPUT like this:
# mptcptrace/src$ cat sf_conn_49
# 2,0,0
# 2,1,1				// this sf should contain datafin
# 2,0,0
# 2,0,0
# 3,2026,3640			// last line is isf!

# OUTPUT: (only consider mpconn having 2 sf)
#	isolate each conn into pcapExtract/ folder
#	fraction of data via each sf
#	rtt of each sf ;	use tcptrace

import os
import glob
import re
from subprocess import call
from IPy import IP
import pathmanager
import numpy as np
import matplotlib.pyplot as plt

STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IP_SRC = 3
IP_DST = 4
SPORT = 5
DPORT = 6
IPVER = 7

colID 	= {}
rtt 	= {}
RTT_avg = []
rttFile = "rtt-tcp.csv"


from collections import namedtuple
ConnPattern = namedtuple("ConnPattern", "name r1 r2 data1 data2")

DualStack= ConnPattern(name='Dual Stack', r1=[],r2=[],data1=[],data2=[])
NPorts = ConnPattern(name='2diffports', r1=[],r2=[],data1=[],data2=[])
Mesh = ConnPattern(name='Mesh',r1=[],r2=[],data1=[],data2=[])

def loadRTT():
	for l in open(rttFile):
		if l[0]=='#':
			continue

		col = l.strip().split(',')
		if len(col) < 40:
			continue

		if col[0] == 'conn_#':
			field = col

			for id in range(len(field)):
				# print id, field[id]	#this will give the name of all parameters provided by tcptrace csv
				colID [field[id]] = id
			continue

		# #  this is IPv4-comp (::91.109.28.55), tcptrace incorrectly print, 
		#  IPy.IP failed to parse, even socket.inet_aton(addr) also failed
		#if col[1]==':55ef:e3b3' or col[1]==':94.254.144.59':
		if re.match( '^:', col[1]):
			sIP = IP(":" + col[1])
		else: 
			sIP = IP(col[1])	# str to IP, using IPy.IP package

		dIP = IP(col[2])
		sport = col[3]
		dport = col[4]

		tuple = (sIP, dIP, sport, dport)		# beware that they are string!
		# print tuple
		# rtt_avg: colID = 95 
		rtt_avg =  float( col[colID['RTT_avg_b2a'] ]) 

		if rtt_avg > 0:
			rtt[tuple] = rtt_avg 


def process(dir, f, extract = False):
	s = f.split('_')
	conn_id = s[2]
	sfs = []
	totalByteC2S = 0
	totalByteS2C = 0

	for l in open(f,'r'):
		sf = l.strip().split(',')
		if l[0] == '#':
			continue
		sfs.append(sf)

	isf = sfs[-1]	# get the last element

	# remove sf not finished 3wHS, except isf
	#	# SYN =1, SYNACK = 2,  ACK = 3
	#sfs[:] = [sf for sf in sfs if (int(sf[STATE]) == 3 or sf == isf ) ]
	#	filter out the empty-data subflows
	sfs[:] = [sf for sf in sfs if ( int(sf[BYTE_S2C]) > 0 ) ]

	if len(sfs) != 2:
		return

	for sf in sfs:
		totalByteC2S += long(sf[BYTE_C2S])
		totalByteS2C += long(sf[BYTE_S2C])

	if totalByteS2C < 1000000:
		return


	pm = pathmanager.find_pm(sfs)
	sf1= sfs[0]
	sf2= sfs[1]
#	ip1ver =IP(sf1[IP_SRC]).version()
#	ip2ver =IP(sf2[IP_SRC]).version()
	ip1ver = int(sf1[IPVER])
	ip2ver = int(sf2[IPVER])

	kind = DualStack
	# if IPv4/v6 dual stack
	if pm == 'MESH' and ip1ver != ip2ver:
		# for plotting IPv4 subflow on x axis: switch 2 subflows if sf1 is IPv6
		if ip1ver == 6:
			sf1=sfs[1]
			sf2=sfs[0]
	if pm == 'NPORTS':
		kind = NPorts
	if pm == 'MESH' and ip1ver == ip2ver:
		kind = Mesh


	rtt1 = float(getRTT(sf1))
	rtt2 = float(getRTT(sf2))

	# if verbose:
		# print '\n', conn_id
		# call('cat '+ f, shell=True)
		# print "RTT:", rtt1, rtt2

	kind.r1.append(rtt1)
	kind.r2.append(rtt2)
	kind.data1.append(long(sf1[BYTE_S2C]))
	kind.data2.append(long(sf2[BYTE_S2C]))
#		if rtt2>0:
#			kind.rtt_ratio.append(rtt1/rtt2)
#			kind.data_ratio.append(long(sf1[BYTE_S2C])/long(sf2[BYTE_S2C]))

#		of2.write(str(conn_id) + '\t' +str(totalByte)+ '\t'+ isf[STATE] +'\n')
	if extract:
		ExtractConnection(dir,conn_id, sf1, sf2)

		getGraphs(dir,conn_id)

def neighborhood(iterable):
    iterator = iter(iterable)
    prev = None
    item = iterator.next()  # throws StopIteration if empty.
    for next in iterator:
        yield (prev,item,next)
        prev = item
        item = next
    yield (prev,item,None)

import math
from decimal import *
from scipy.stats import binned_statistic
# real time data sent over each subflow
dataSent = {}

def getDataDistribution(dir, timeslot):
							# in ms
	print(os.getcwd() + "\n")
	f = open(str(dir) + '/s2c_seq_1.xpl' ,'r')

	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('line'):
			l = line.strip().split(' ')
			# this causes rounding error (sai so lam tron)
			# better to use Decimal type, but np.digitize/scipystats only support float type
			time = float(l[1])
			bytes = long(l[4]) - long(l[2])	# Ack# - Seq#
			sf = int(prev)
			if sf not in dataSent:
				dataSent[sf] = []
			dataSent[sf].append([time, bytes])
	print len(dataSent[1])

	mp_start = mp_end = dataSent[1][0][0]

	for sf in dataSent:
		data = np.array(dataSent[sf])
		time = data[:,0] # get the first column
		data[time.argsort()] # sort by time

		x_time = data[:,0]
		bytes = data[:,1]
		start = min(x_time)
		end   = max(x_time)
		mp_start = min(start, mp_start)
		mp_end   = max(end, mp_end)

#		what i want is to binning on time (keys) and then compute on bytes(values)
#		cannot use  np.digitize
#		digitized = np.digitize(x_time, bins)
	# plot individually
		# plt.figure()
		# plotByte(x_time, bytes, start, end, timeslot)


	for sf in dataSent:
		data = np.array(dataSent[sf])
		x_time = data[:,0]
		bytes  = data[:,1]
		print mp_start, mp_end, timeslot
		plt.figure(1)	# specify the common figure
		plotByte(x_time, bytes, mp_start, mp_end, timeslot)

	plt.show(block=False)
	plt.savefig('Bytes')

def plotByte(x_time, bytes, start, end, timeslot):
	bins = np.arange(start, end, step = (timeslot))

	print len(x_time), len(bins), start, end

	bin_sums = binned_statistic(x_time, bytes, statistic='sum', bins=len(bins), range=[(start, end)] ) [0]
	print bin_sums
	plt.plot(bins, bin_sums)


def ExtractConnection(dir,n, sf1, sf2):
	f = open(dir+'stats_'+str(n)+'.csv', 'r')
#	for line in f:
#		l = line.strip().split(';')
#		if l[2]=='tcpDumpFilter':
#			pcapfile = l[0]
#			filter= l[3]

	outdir = 'pcapExtract/'+str(n)
	if not os.path.exists(outdir):
		os.makedirs(outdir)
	pfile = str(outdir)+'/'+str(n)+".pcap"
	if os.path.isfile(pfile):
		return

	print 'extracting pcap ...'

	fi1 = ' ( ip'+ ( sf1[IPVER] if sf1[IPVER] == '6' else '')
	fi2 = ' ( ip'+ ( sf2[IPVER] if sf2[IPVER] == '6' else '')
	fi1 +=' host '+ sf1[IP_SRC] + ' && tcp port '+sf1[SPORT] + ' ) '
	fi2 +=' host '+ sf2[IP_SRC] + ' && tcp port '+sf2[SPORT] + ' ) '
	filter = ' tcp && ( '+fi1 + '||' + fi2 +' ) '
	#print filter

	l = f.readline().strip().split(';')	#read the first line
	start = l[1]
	end   = l[2]
	print start, end
	if start == end:
		call('sudo tcpdump -r '+ start +' -w ' + pfile + " \'"+filter+"\' ", shell=True)
#	else:
#		for trace in sorted(glob.glob("*.pcap")):
#			if str(trace) > start and str(trace)< end:
#				call('sudo tcpdump -r '+trace +' -w pcapExtract/'+str(n)+".pcap \'"+filter+"\'", shell=True)

	print '...'

def getGraphs(dir, n):
	outdir = 'pcapExtract/'+str(n)
	os.chdir(outdir)
	tcptrace = 'tcptrace -R -T -S -n '+ str(n)+'.pcap'
	print tcptrace
	call(tcptrace +'> /dev/null', shell=True)
	call("rm  a2b_* c2d_* ", shell=True)

	mp = 'mptcptrace -s -S -F 1  -f '+ str(n)+'.pcap'
	print mp
	call(mp, shell=True)
	call("rm  c2s* mptcptrace*  *tput* s2c_rightEdge_1.xpl ", shell=True)

#	getDataDistribution(".", 0.2)
	os.chdir(dir)


def getRTT(sf):
	tuple = (IP(sf[IP_SRC]), IP(sf[IP_DST]), sf[SPORT], sf[DPORT])
	if tuple in rtt:
		return rtt[tuple]
	else:
		print  sf, 'not in tcptrace output, or RTT = 0'
		return 0

def main(dir, outdir, extract, plot=False):
	# use tcptrace to get RTT. Run tcptrace if rttFile does not exist.
	if not os.path.isfile(rttFile) or os.path.getsize(rttFile)<1000:
		# print 'Extract RTT with tcptrace'
		call('tcptrace --csv -l -r -nt *.pcap > rtt-tcp.csv 2> /dev/null', shell=True)
	loadRTT()
	print 'rttfile len:',len(rtt)
	# of1 = file(outdir +'/'+'2sf-nports.txt','w+')
	# of2 = file(outdir +'/'+'2sf-dual-stack.txt','w+')
	# of3 = file(outdir +'/'+'2sf-mesh-IPv4.txt','w+')

	if extract and not os.path.exists('pcapExtract/'):
		os.makedirs('pcapExtract/')

	for f in sorted(os.listdir(dir)):
		if f.startswith('sf_conn_'):
			process(dir, f, extract)

	if extract:
		os.chdir('pcapExtract')
		call('grep "bytesReinjected" */stats_1.csv > reinjected.dat', shell=True)

	if plot:
		doplot(DualStack)
		doplot(NPorts)
		doplot(Mesh)

def doplot(kind):
	plt.figure()

	plt.subplot(2,1,1)
	plt.scatter( kind.r1, kind.r2, s=3, label='rtt')
	plt.xlim(0,3000 )
	plt.ylim(0,3000 )
	plt.plot([0, 3000], [0, 3000], 'k-', lw =0.2)
	plt.gca().set_aspect('equal', adjustable='box')
	plt.title(kind.name)

	ax = plt.subplot(2,1,2)
	plt.scatter( kind.data1, kind.data2, s=3, label='data')
	ax.set_xscale('log')
	ax.set_yscale('log')
	plt.gca().set_aspect('equal', adjustable='box')
	plt.xlim(1000, 10000000000)
	plt.ylim(1000, 10000000000)
#	plt.gca().set_xlim(left=0)
#	plt.gca().set_ylim(ymin=0)
	plt.plot([1000, 1000000000], [1000, 1000000000], 'k-', lw =0.25)
	plt.show(block=False)

	print 'samples nb:', len (kind.data1)

if __name__ == '__main__':
	dir = '../'
	main(dir,os.getcwd())
