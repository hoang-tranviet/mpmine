#! /usr/bin/env python

# Plot receive window, and flight size from s2c_flight_1.xpl

import os
import re
from subprocess import call
import numpy as np
from datetime import datetime
from decimal import Decimal
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description="Stats for receive window")
parser.add_argument('--dir', '-d',	default="./")
parser.add_argument('--xrange'  , '-x', help="the right limit on x axis", default= 0)
parser.add_argument('--yrange'  , '-y', help="the top   limit on y axis", default= 0)

parser.add_argument('--reinject', '-r', help="the horizontal line showing reinjections", default= 0)
parser.add_argument('--verbose' ,'-v', default= False, action='store_true')
parser.add_argument('--logscale','-l', default= False, action='store_true')
args = parser.parse_args()

RWIN   = 1
FLIGHT = 2

MARK = 0
TIME = 1 	# column No
SIZE = 2

# in stats_*.csv
TYPE = 2

def neighborhood(iterable):
    iterator = iter(iterable)
    prev = None
    item = iterator.next()  # throws StopIteration if empty.
    for next in iterator:
        yield (prev,item,next)
        prev = item
        item = next
    yield (prev,item,None)


def getReinjection(dir):

	reinjection = []

	f = open(str(dir) + '/s2c_seq_1.xpl' ,'r')
	lines = f.readlines()

	for prev,line,next in neighborhood(lines):

		if line.startswith('atext') and next.strip() == 'R':
			# This is a reinjection
			l  = line.strip().split(' ')
			time = Decimal(l[TIME])
			seq  = l[2]
			sf = prev			

			reinjection.append(time)

	return reinjection


def getRWind(dir):

	# loading rwin and flightsize
	rwin = []
	flight = []
	file = str(dir) + '/s2c_flight_1.xpl'
	if not os.path.isfile(file):
		return 0
	f = open(file ,'r')

	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('diamond'):
			l = line.strip().split(' ')
			# float type causes rounding error (sai so lam tron)
			# better to use Decimal type, but np.digitize/scipystats only support float type
			# time = Decimal(l[1])
			time = Decimal(l[1])
			size = int(l[2])
			type = int(prev)
			if type == RWIN:
				rwin.append((time,size))
			elif  type == FLIGHT:
				flight.append((time,size))
	# print 'rwind and flight:', len(rwin), len(flight)

	return rwin, flight


def immediate_subdir(a_dir):
    return [name for name in sorted(os.listdir(a_dir))	if os.path.isdir(os.path.join(a_dir, name))]


def main(dir):
	rwin, flight = getRWind(dir)
	reinject = getReinjection(dir)
	plotline(rwin, flight, reinject)


def plotline(rwin, flight, reinject):

	# fig, [rx, ax] = plt.subplots(2, sharex=True, sharey=False )
	fig, ax = plt.subplots()


	x=[s[0] for s in rwin]
	y=[s[1] for s in rwin]
	# relative time: x axis start at zero
	begin = x[0]
	x = [(s-begin) for s in x]
	plt.plot( x, y, lw=1.5, label='receive window')


	x=[s[0] for s in flight]
	y=[s[1] for s in flight]
	begin = x[0]
	x = [(s-begin) for s in x]
	plt.plot( x, y, lw=1.5, color = 'orange', label='MPTCP flight size')


	print len(reinject)
	reinject = [(s-begin) for s in reinject]

	# # Plot reinjections on a Horizontal line y
	# y = [0] * len(reinject)
	# rx.plot( reinject, y, lw=0, color = 'red', marker = 'x', label='reinjections')
	# rx.set_axis_off()
	# rx.legend(loc='best')

	rline = int(args.reinject)
	y = [rline] * len(reinject)
	ax.plot( reinject, y, lw=0, color = 'red', marker = 'x', mew = 1.5, label='reinjections')


	if args.logscale:
		ax.set_xscale('log')

	plt.xlabel('Time')
	plt.ylabel('Size (bytes)')

	if args.xrange:
		plt.xlim(0,float(args.xrange))
	if args.yrange:
		plt.ylim(0,float(args.yrange))


	ax.legend(loc='best')
	path, file = os.path.split(os.getcwd())

	print path, file 
	plt.savefig('rwind-'+file+'.pdf')

	plt.show()



if __name__ == '__main__':
	dir = args.dir
	main(dir)