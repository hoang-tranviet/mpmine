#! /usr/bin/env python

#ongoing writing

import os
from subprocess import call
import numpy as np
import matplotlib.pyplot as plt

import argparse
parser = argparse.ArgumentParser(description="Get RTTs from TCPtrace's RTT graph")

parser.add_argument('--dir', '-d',	default="./")
parser.add_argument('--all', '-a', help="analyse all connections in folder", default= False, action='store_true')
parser.add_argument('--verbose','-v', default= False, action='store_true')
args = parser.parse_args()


MARK = 0
TIME = 1 	# column No
VALUE= 2

def loadRTT(dir, rttfile):

	RTTs=[]

	file = str(dir) + rttfile
	if not os.path.isfile(file):
		return(0)
	for line in open(file ,'r'):
		if line.startswith('dot'):
			l = line.strip().split(' ')
			rtt_val = float(l[VALUE])
			RTTs.append(rtt_val)


	
	return min(RTTs), max(RTTs), max(RTTs)-min(RTTs), int(np.average(RTTs)), np.median(RTTs), np.percentile(RTTs, 90)

# from RichieHindle, stackoverflow
def immediate_subdir(a_dir):
    return [name for name in sorted(os.listdir(a_dir))	if os.path.isdir(os.path.join(a_dir, name))]

def main(dir):
	jitter = []
	minRTT = []
	maxRTT = []
	rttdiff= []

	if (args.all):
		rtt1 =loadRTT(dir, '/b2a_rtt.xpl')
		rtt2 =loadRTT(dir, '/d2c_rtt.xpl')

		jitter.append(rtt1[2])
		jitter.append(rtt2[2])

		diff = rtt2[3] - rtt1[3]
		rttdiff.append(diff)

		doplot(data)
		
	else:
		rtt1 =loadRTT(dir, '/b2a_rtt.xpl')
		rtt2 =loadRTT(dir, '/d2c_rtt.xpl')

		jitter.append(rtt1[2])
		jitter.append(rtt2[2])

		diff = rtt2[3] - rtt1[3]
		rttdiff.append(diff)

		doplot(data)

def doplot(data):

	fig, ax = plt.subplots()

	x=np.sort(data)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='Round-trip time')

	ax.set_xscale('log')

	ax.legend()
	plt.show()
	plt.savefig('out/stats-rtt.eps', bbox_inches='tight')



if __name__ == '__main__':
	dir = args.dir
	main(dir)