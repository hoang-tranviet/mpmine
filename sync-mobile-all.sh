#!/bin/bash

# This script sync all traces on sockproxy, including traffic to phones and traffic to remote servers
# Currently not used

cd ../../imc/ns328523.ip-37-187-114.eu/
rsync -e "ssh -i ~/.ssh/id_rsa"  --ignore-existing  -vhP hoang@130.104.72.210:/home/mpcollect/collect/ns328523.ip-37-187-114.eu/*.xz  .

# todo: if trace not exist -> unzip if did not -> copy trace
for zipfile in *.xz 
do
	trace=${zipfile%.tar.xz}
	echo $trace
	
	file=${trace}.pcap
	# copy trace from subfolder if it doesn't exist.
	if [ ! -f $file ]; then
		# if folder not exist, unzip
		if [ ! -d "$trace" ]; then 
			echo "folder not exist: ${trace}"
			tar --keep-old-files -xvf  ${zipfile}
		fi
		cd ${trace}
		editcap  -F libpcap  *.pcap  ../${trace}.pcap
		cd ..
		# remove folder
		rm -r $trace
	fi
done
