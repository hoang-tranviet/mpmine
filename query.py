#! /usr/bin/env python

# Script to query connections having desired size (in bytes)
import os
from subprocess import call

f = 'mp-data.txt'
separator='\t'

result = []
for line in open(f):

	l = line.strip().split(separator)
	if line[0]=='#':
		continue
	if len(l)<2:
		continue

	bytes = float(l[1])

	# # query connections in highly concentrate zone of CDF bytes
	# # result: port 80
	# if bytes > 4000 and bytes < 10000:

	# # query connections larger than 1 GB
	# # result: 22 of FTP, 7 of Iperf
	if bytes > 1000000000:
		print l[1]
		call('cat ../stats_'+l[0]+'.csv | grep "sf_0_port;"', shell=True)
		result.append(l)

print len(result)