#! /usr/bin/env python

# INPUT: stats_*.csv

# OUTPUT:
#	mptcp conn size (in Bytes)

import os
import argparse
from subprocess import call
TRACE = 0
CONN  = 1
TYPE  = 2
VAL1  = 3
VAL2  = 3

data = []


def process(dir, f, of):
	s = f.split('_')
	conn_id = s[1]

	for line in open(dir+f):
		l = line.strip().split(';')

		if l[TYPE] == 'seqAcked':
			bytes = long(l[VAL1]) + long(l[VAL2])
			if bytes >= 4:
				bytes -= 4
			else:
				bytes = 0
			data.append( bytes )
			of.write(str(f) +'\t' + str(bytes) + '\n')

			return

def main(dir):
	of = open('conn_size.txt', 'w')

	for f in os.listdir(dir):
		if f.startswith('stats_'):
			process(dir, f, of)

	print  len(data)

	doplot()

def doplot():
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()

	x=np.sort(data)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.8)
	ax.set_xscale('log')

	plt.xlabel('Connection size (payload, in Bytes) ')
	plt.grid()
	plt.savefig('conn_size.pdf')
	plt.show()

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "get connection size from stats_*.csv")
	args = parser.parse_args()

	dir = '../'
	main(dir)
