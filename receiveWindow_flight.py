#! /usr/bin/env python

# Stats for receive window, on extracted connections with 2 sf and > 1MB.
# This stats is done manually, input from s2c_flight_1.xpl so it's pretty slow.

import os
import re
from subprocess import call
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description="Stats for receive window")
parser.add_argument('--dir', '-d',	default="./")
parser.add_argument('--all', '-a', help="analyse all connections in folder", default= False, action='store_true')
parser.add_argument('--verbose','-v', default= False, action='store_true')
parser.add_argument('--logscale','-l', default= False, action='store_true')
args = parser.parse_args()

RWIN   = 1
FLIGHT = 2

MARK = 0
TIME = 1 	# column No
SIZE = 2

# in stats_*.csv
TYPE = 2

def neighborhood(iterable):
    iterator = iter(iterable)
    prev = None
    item = iterator.next()  # throws StopIteration if empty.
    for next in iterator:
        yield (prev,item,next)
        prev = item
        item = next
    yield (prev,item,None)


def getBytesAcked(dir):
	# get size of connection (amount of bytes acked)
	file = str(dir) + '/stats_1.csv'
	if not os.path.isfile(file):
		return(0, 0, 0)	

	for line in open(file):
		l = line.strip().split(';')

		if l[TYPE] == 'seqAcked':
			bytesAcked = int(l[4])
			return bytesAcked

def getRWind(dir):

	# loading rwin and flightsize
	rwin = {}
	# flight = {}
	file = str(dir) + '/s2c_flight_1.xpl'
	if not os.path.isfile(file):
		return(0, 0, 0)
	f = open(file ,'r')

	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('diamond'):
			l = line.strip().split(' ')
			# float type causes rounding error (sai so lam tron)
			# better to use Decimal type, but np.digitize/scipystats only support float type
			# time = Decimal(l[1])
			time = l[1]
			size = int(l[2])
			type = int(prev)
			if type == RWIN:
				rwin[time] = size
			# elif  type == FLIGHT:
			# 	flight[time] = size
	# print 'rwind and flight:', len(rwin), len(flight)

	bytesAcked = getBytesAcked(dir)

	return 1, bytesAcked, ( max(rwin.values()) >> 10)


def immediate_subdir(a_dir):
    return [name for name in sorted(os.listdir(a_dir))	if os.path.isdir(os.path.join(a_dir, name))]


def main(dir):
	if (args.all):
		maxwinds = []

		for conn_dir in immediate_subdir(dir):
			print conn_dir+'/'
			(ConnExists, bytes, maxwin) = getRWind(conn_dir)

			if ConnExists:
				print bytes, maxwin
				maxwinds.append((bytes, maxwin))
				# of.write( l[VAL] + '\n')

		print len(maxwinds)
		plotscatter(maxwinds)
		plotcdf(maxwinds)

	else:
		print getRWind(dir)


def plotscatter(data):

	data = sorted(data)

	fig, ax = plt.subplots()

	x=[s[0] for s in data]
	y=[s[1] for s in data]

	plt.scatter( x, y, s=3, label='receive window size')

	if args.logscale:
		ax.set_xscale('log')

	plt.xlabel('bytes Acked')
	plt.ylabel('receive window size (KB)')

	ax.legend(loc='best')
	plt.savefig('stats-rwind-scatter.pdf')


def plotcdf(data):

	x=[s[1] for s in data]

	fig, ax = plt.subplots()

	x=sorted(x)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='receive window size')

	if args.logscale:
		ax.set_xscale('log')

	ax.legend(loc='best')
	plt.savefig('stats-rwind.pdf')
	plt.show()



if __name__ == '__main__':
	dir = args.dir
	main(dir)