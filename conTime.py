#! /usr/bin/env python

# INPUT: stats_*.csv

# OUTPUT:
#	conn duration

import os
import argparse
from subprocess import call
TRACE = 0
CONN  = 1
TYPE  = 2
VAL   = 3

durations = []


def process(dir, f, of):
	s = f.split('_')
	conn_id = s[1]

	for line in open(dir+f):
		l = line.strip().split(';')

		if l[TYPE] == 'conTime':
			duration = float(l[VAL])
			durations.append( duration )
			of.write(str(f) +'\t' + l[VAL] + '\n')
			if duration > 10000:
				print f, duration
			return

def main(dir, plot=False):
	of = open('conTime.txt', 'w')

	for f in os.listdir(dir):
		if f.startswith('stats_'):
			process(dir, f, of)

	print  len(durations)
	if plot:
		doplot()

def doplot():
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()

	x=np.sort(durations)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.8)
	ax.set_xscale('log')
	ax.set_xlim(xmin=0.001)
	plt.xlabel('Connection duration (s)')
	plt.ylabel('CDF')
	plt.grid()
	plt.savefig('conn_duration.pdf')
	plt.show()

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "get connection duration")
	parser.add_argument('--plot', '-p', default= False, action='store_true')
	args = parser.parse_args()

	dir = '../'
	main(dir,args.plot)
