#! /usr/bin/env python

# INPUT like this:
# MacBook-Pro-2009:delay hoang$ cat conn_40 
#
# 931,1407352272.625263,isf,2001:200:164:33:10:11:10:2,2001:6a8:3080:1:216:3eff:fec5:c815,v6
# 934,1407352272.920091,add_addr,1,10.11.10.2,1
# 935,1407352272.920114,add_addr,1,130.104.230.45,0
# 937,1407352273.214869,add_addr,2,203.178.154.50,1
# 938,1407352273.215025,join_syn,2,203.178.154.50,130.104.230.45,36040,80
# 940,1407352273.215390,join_syn,10,2001:200:164:48::53:1,2001:6a8:3080:1:216:3eff:fec5:c815,53511,80
# 949,1407352274.746723,add_addr,10,2001:200:164:48::53:1,1

# OUTPUT: 1) show stats of fallback: conns, IPs
#		  2) extract conns having fallback.
import os
import argparse
from IPy import IP
from subprocess import call
from pprint import pprint	# sort dict by key and prettyprint

IDX = 0
TS  = 1
TYPE= 2
IPSRC = 3
IPDST = 4
SPORT =	5
DPORT =	6

extract = 1

falls_per_ip   = {}
falls_per_port = {}

def process(f, of1):

	for l in open(args.dir + f):

		## access a character in a string by index is very slow!
		## by commenting out, script runtime reduced from 36s -> 2s
		## may use regex if we need checking
		# if l[0] == '#':
		# 	continue
		col = l.strip().split(',')
		if len(col) <= TYPE:
			continue

		if col[TYPE]=='tracefile(s)':	# from client
			tuple = ( IP(col[IPSRC]), IP(col[IPDST]), col[SPORT], col[DPORT])
			of1.write(str(f)+'\t' +l + '\n')

		if col[TYPE]=='fallback':
			tuple = ( IP(col[IPSRC]), IP(col[IPDST]), col[SPORT], col[DPORT])
			of1.write(str(f)+'\t' +l + '\n')

			return tuple

	return None

def ExtractConnection(outdir,n):
	statsFile = open('stats_'+ n +'.csv')
	outtrace = outdir + n + '.pcap'
	for l in statsFile:
		col = l.split(';')

		if col[0] == 'tracefile(s)':
			trace = col[1]
			
		if col[2] == 'tcpDumpFilter':
			filter= col[3]
			# print col
			print('sudo tcpdump -r '+ trace +'  -w ' + outtrace + " \'"+filter+"\' ")
			call('sudo tcpdump -r '+ trace +'  -w ' + outtrace + " \'"+filter+"\' ", shell=True)

# def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
#     return [int(text) if text.isdigit() else text.lower()
#             for text in re.split(_nsre, s)]  

def main(dir, plot=False):

	print 'searching for fall-backs'
	of1 = open('out/fallback.txt','w+')

	outdir = 'out/fallbacks/'
	if extract and not os.path.exists(outdir):
		os.makedirs(outdir)

	fallconn=[]

	for f in os.listdir(dir):
		if f.startswith('conn_'):
			fall= process(f, of1)

			if fall != None:
				print f
				fallconn.append(fall)

				if extract ==1:
					connID = f.split('_')[1]
					ExtractConnection(outdir,connID )

	for (ipsrc, ipdst, sport, dport) in fallconn:
		for ip in [ipsrc, ipdst]:
			if ip not in falls_per_ip:
				falls_per_ip[ip] = 1
			else:
				falls_per_ip[ip] += 1

		for p in [sport, dport]:
			if p not in falls_per_port:
				falls_per_port[p] = 1
			else:
				falls_per_port[p] += 1

	pprint(falls_per_ip)
	pprint(falls_per_port)

	for i in sorted(falls_per_ip, key=falls_per_ip.get):
		print i, falls_per_ip[i]
	print 'number of IPs having fallback:', len(falls_per_ip) - 2 	# exclude 2 IPs of our server

	print 'number of conns having fallback:', len(fallconn)

if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "searching for fall-backs")

	parser.add_argument('--dir', '-d',	default="./")
	parser.add_argument('--plot', '-e', default= False, action='store_true')
	args = parser.parse_args()

	main(args.dir)