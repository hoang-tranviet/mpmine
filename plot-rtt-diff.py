#! /usr/bin/env python

# INPUT: out/rtt-diff.txt (this is inturn output of rtt-diff.py)

# OUTPUT:
#	plot diff between fastest SF and slowest SF of a MPTCP conn.
#	can set condition

import os
import argparse
from subprocess import call
 
BYTES  = 1
RTTMIN = 2
RTTMAX = 3
RTTDIFF= 4
RTT_2ND_MIN = 5

rttdiff = []


def getData(f):
	best_RTT 		= []
	second_best_RTT = []
	rttdiff 	    = []
	rttdiff_ratio   = []
	for l in f:
		if l.startswith('#'):
			continue
		conn = l.strip().split('\t')
		size = float(conn[BYTES])
		value  = float(conn[RTTDIFF])
		best_rtt = float(conn[RTTMIN])
		second_bestrtt = float(conn[RTT_2ND_MIN])

		if len(conn) > 2 and size > 100000:
			rttdiff.append(value)
			if best_rtt > 0:
				rttdiff_ratio.append(value/best_rtt)
				best_RTT.append(best_rtt)
				second_best_RTT.append(second_bestrtt)

			if value > 8000:
				print conn
	return rttdiff, rttdiff_ratio, best_RTT, second_best_RTT


def main(dir, outdir ):
	print os.getcwd()
	f2 = open(dir +'rtt-diff.txt','r+')

	rttdiff, rttdiff_ratio, best_RTT, second_best_RTT = getData(f2)


#plot rtt_diff
	import numpy as np
	import matplotlib.pyplot as plt

	print len(rttdiff)

	fig, ax = plt.subplots()

	x=np.sort(rttdiff)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, lw=1.5, color = 'blue')
	
	ax.set_xscale('log')
	ax.set_xlim(xmin= 0.1)
	ax.grid()
	plt.xlabel('Difference of average RTT between worst and best subflows (ms)')
	plt.ylabel('CDF')
	plt.savefig(outdir +'rtt-diff.pdf', bbox_inches='tight')
	plt.show(block=False)


# # plot rttdiff_ratio
# 	print len(rttdiff_ratio)

# 	fig, ax = plt.subplots()

# 	x=np.sort(rttdiff_ratio)
# 	cdf=np.arange(len(x))/float(len(x))
# 	ax.plot( x, cdf, lw=1.5, color = 'violet')
	
# 	ax.set_xscale('log')
# 	ax.set_xlim(xmin= 0.001, xmax=100)
# 	ax.grid()
# 	plt.xlabel('Ratio of RTT Difference and best subflow\'s RTT ')
# 	plt.ylabel('CDF')
# 	plt.savefig(outdir +'rtt-diff-ratio.pdf', bbox_inches='tight')
# 	plt.show(block=False)


# # scatter plot bestRTT and second_best_RTT
# 	print len(best_RTT)

# 	fig, ax = plt.subplots()

# 	ax.scatter( best_RTT, second_best_RTT, s=0.9, color = 'red')
	
# 	# ax.set_xscale('log')
# 	ax.set_xlim(xmin =0, xmax=10000)
# 	ax.set_ylim(ymin= 0, ymax=10000)
# 	ax.grid()
# 	plt.xlabel('RTT of best subflow')
# 	plt.ylabel('RTT of second best subflow')
# 	plt.savefig(outdir +'rtt-2-best-subflows.pdf', bbox_inches='tight')
# 	plt.show()


# box plot bestRTT and second_best_RTT

	print len(best_RTT)
	keys = best_RTT
	values= second_best_RTT
	second_best = dict(zip(keys, values))

	binsize = 40
	bins = 15
	box = {}
	for key in keys:
		index = int(key/binsize)
		print index, key

		if index not in box:
			box[index] = [(second_best[key])]
		else:
			box[index].append( second_best[key] ) 

	data = box.values()
	fig, ax = plt.subplots()

	
	ax.boxplot(data[0:bins], sym='')

	xtickNames = []
	for i in range(bins):
		lower= str(i*binsize)
		upper= str((i+1)*binsize)
		xtickNames.append( str (lower+' - '+upper) )

	xticklabels = plt.setp(ax, xticklabels=xtickNames )
	plt.setp(xticklabels, rotation=60, fontsize=10)

	# ax.set_xscale('log')
	ax.set_xlim(xmin =0, xmax=bins+1)
	ax.set_ylim(ymin= 0, ymax=4000)
	ax.grid()
	plt.xlabel('RTT of best subflow (ms)', labelpad=15)
	plt.ylabel('RTT of second best subflow (ms)')
	plt.savefig(outdir +'rtt-2-best-subflows-boxplot.pdf', bbox_inches='tight')
	plt.show()

if __name__ == '__main__':
	dir = './'
	main(dir, dir)
