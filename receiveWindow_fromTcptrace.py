#! /usr/bin/env python

# Stats for receive window from client, on all TCP connections, input from TCPtrace long format file: rtt-tcp.csv.
# Help: to run where rtt-tcp.csv exists.

import os
import re
from subprocess import call
import numpy as np
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description="Stats for receive window")
parser.add_argument('--dir', '-d',	default="./")
parser.add_argument('--all', '-a', help="analyse all connections in folder", default= False, action='store_true')
parser.add_argument('--verbose','-v', default= False, action='store_true')
parser.add_argument('--logscale','-l', default= False, action='store_true')
args = parser.parse_args()

#INPUT:  tcptrace long format csv, with RTT option
#OUTPUT: RTT column (col 95 in tcptrace csv output)


maxwinds =[]
colID ={}

def loadRwinMax():
	ackPktC2S = 11

	f = "rtt-tcp.csv"
	of = open("rwindmax.txt", "w+")

	of.write('#unique_bytes_sent_b2a \t rwindmax \n')	


	for l in open(f):
		if l[0]=='#':
			continue

		col = l.strip().split(',')
		if len(col) < 40:
			continue

		if col[0] == 'conn_#':
			# print len(col)
			field = col

			for id in range(len(field)):
				# print id, field[id]
				colID [field[id]] = id
			continue

		# if int(col[ackPktC2S]) < 3:
		# 	continue

		# sIP = col[1]
		# dIP = col[2]
		# sport = col[3]
		# dport = col[4]

		# tuple = (sIP, dIP, sport, dport)		# beware that they are string!

		# unique_bytes_sent_a2b = int(col[colID['unique_bytes_sent_a2b']])
		unique_bytes_sent_b2a = int(col[colID['unique_bytes_sent_b2a']])

		if unique_bytes_sent_b2a <1 :
			continue

		rwin_a2b =  int( col[colID['max_win_adv_a2b'] ]) 
		# rwin_b2a =  int( col[colID['max_win_adv_b2a'] ]) 


		# print unique_bytes_sent_b2a,rwin_a2b
		maxwinds.append((unique_bytes_sent_b2a, rwin_a2b))

		of.write(str(unique_bytes_sent_b2a) + '\t' + str(rwin_a2b) + '\n')	


def main(dir):
	loadRwinMax()
	print len(maxwinds)
	# plotscatter(maxwinds)
	# plotcdf(maxwinds)
	plotbox(maxwinds)

def plotscatter(data):

	data = sorted(data)

	fig, ax = plt.subplots()

	x=[s[0] for s in data]
	y=[(s[1]) for s in data]

	if args.logscale:
		ax.set_xscale('log')
		ax.set_yscale('log')

	plt.scatter( x, y, s=0.5, label='max receive window size')

	plt.xlabel('Unique bytes sent')
	plt.ylabel('Max receive window size')

	ax.legend(loc='best')
	plt.savefig('rwindmax-scatter.pdf')

def plotbox(data):
	import math

	boxes ={}
	for s in data:
		bytes = s[0]
		rwin  = s[1]

		# put rwin into the correspond bin: 1->10KB, 10K->100K, 100K->1MB, 1MB-10MB,...
		index = int(math.log10(bytes))

		# if index <4:
		# 	index = 4	# firs bin include all conns < 100KB

		if index > 8:
			index = 8	# last bin include all conns > 100MB

		if index not in boxes:
			boxes[index]=[(rwin)]
		else:
			boxes[index].append(rwin)

	data =[]
	for index in boxes:
		print index, len(boxes[index]), np.median(boxes[index])
		data.append(boxes[index])

	fig, ax = plt.subplots(figsize=(10,6))
	fig.canvas.set_window_title('A Boxplot Example')
	plt.subplots_adjust(left=0.075, right=0.95, top=0.9, bottom=0.25)

	# bp = plt.boxplot(data, notch=0, sym='+', vert=1, whis=1.5)
	bp = plt.boxplot(data[4:], notch=0, sym='+', vert=1, whis=1.5)

	if args.logscale:
		ax.set_yscale('log')

	plt.setp(bp['boxes'], color='black')
	plt.setp(bp['whiskers'], color='black')
	plt.setp(bp['fliers'], color='red', marker='+')

	xtickNames = plt.setp(ax, xticklabels=['10-100 KB','100KB-1MB', '1-10 MB', '10-100 MB', 'Larger than 100 MB'])
	plt.setp(xtickNames, rotation=30, fontsize=12)

	plt.xlabel('Unique bytes sent', fontsize=12)
	plt.ylabel('Max receive window size', fontsize=12)

	plt.savefig('rwindmax-boxplot.pdf')

	plt.show()

def plotcdf(data):

	x=[s[1] for s in data]

	fig, ax = plt.subplots()

	x=sorted(x)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='max receive window size')

	if args.logscale:
		ax.set_xscale('log')

	ax.legend(loc='best')
	plt.savefig('rwindmax-cdf.pdf')
	plt.show()



if __name__ == '__main__':
	dir = args.dir
	main(dir)