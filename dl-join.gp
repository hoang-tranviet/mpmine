#!/usr/bin/gnuplot
##!/opt/local/bin/gnuplot

set term postscript enhanced eps color 
#set terminal postscript eps enhanced color "NimbusSanL-Regu" fontfile "/usr/share/texlive/texmf-dist/fonts/type1/urw/helvetic/uhvr8a.pfb"
#set terminal postscript eps enhanced color "NimbusSanL-Regu" fontfile "/usr/local/texlive/2014/texmf-dist/fonts/type1/urw/helvetic/uhvr8a.pfb"
set term postscript font 22

set output "delay-join.eps"

#set size 0.9, 0.7

set logscale x
set format x "10^{%L}"

set xlabel "Delay CAPA-SYN to First Join (s)"
set ylabel "CDF"
#set xrange [:5000]
unset key

plot "delay-join.txt" using 2:(1./91700) lw 3  smooth cumulative
