#! /usr/bin/env python

import os
import re
from subprocess import call
import numpy as np
from datetime import datetime
import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser(description="Auto determine reinjections due to receive window -flight size limit")
parser.add_argument('--dir', '-d',	default="./")
parser.add_argument('--all', '-a', help="analyse all connections in folder", default= False, action='store_true')
parser.add_argument('--verbose','-v', default= False, action='store_true')
args = parser.parse_args()

FLIGHT_DIFF_THRESH = 5000	# bytes, difference between rwnd and flight size
REINJ_DELAY_THRES  = 0.05 	# second, max delay from rwnd_limited to reinjection
RWIN   = 1
FLIGHT = 2

MARK = 0
TIME = 1 	# column No
SIZE = 2

def neighborhood(iterable):
    iterator = iter(iterable)
    prev = None
    item = iterator.next()  # throws StopIteration if empty.
    for next in iterator:
        yield (prev,item,next)
        prev = item
        item = next
    yield (prev,item,None)



def closeEnough(a, b):
	if (a - b) < FLIGHT_DIFF_THRESH:
		return True
	else:
		return False

def analyseReinject(dir):
	print dir+'/'

	file = str(dir) + '/stats_1.csv'
	if not os.path.isfile(file):
		return(0, 0, 0)	
	
	for line in open(file ,'r'):
		if bool(re.search("bytesReinjected;0;0", line)):
			print 'no reinjection'
			return(1, 0, 0)	


	# loading rwin and flightsize

	# real time cwnd/flight size
	rwin = {}
	flight = {}
	file = str(dir) + '/s2c_flight_1.xpl'
	if not os.path.isfile(file):
		return(0, 0, 0)
	f = open(file ,'r')

	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('diamond'):
			l = line.strip().split(' ')
			# float type causes rounding error (sai so lam tron)
			# better to use Decimal type, but np.digitize/scipystats only support float type
			# time = Decimal(l[1])
			time = l[1]
			size = int(l[2])
			type = int(prev)
			if type == RWIN:
				rwin[time] = size
			elif  type == FLIGHT:
				flight[time] = size
	# print 'rwind and flight:', len(rwin), len(flight)

	# detect rwnd limits
	flight_limit = []

	current_rwin = 100000000
	for t in sorted(flight.keys()):

		if (t in rwin):			# if a moment have rwnd sample, there also have flight size :)
			current_rwin = rwin[t]

		if closeEnough(current_rwin, flight[t]):
			# print t, current_rwin, current_rwin - flight[t]
			flight_limit.append(float(t))

	if args.verbose:
		print 'window_limit count:', len(flight_limit)


	# search for Reinjections
	reinjection = []

	f = open(str(dir) + '/s2c_seq_1.xpl' ,'r')
	lines = f.readlines()

	for prev,line,next in neighborhood(lines):
		if line.startswith('atext') and next.strip() == 'R':
			# This is a reinjection
			l  = line.strip().split(' ')
			time = float(l[TIME])
			seq  = l[2]
			sf = prev			

			# print time
			reinjection.append(time)
	print 'Reinjections 		:', len(reinjection)


	# determine which reinjections due to rwnd limit
	oppor_reinject=[]



	# for t_r in reinjection:
	# 	# print t_r, datetime.fromtimestamp(t_r).strftime('%M:%S.%f')
	# 	for t_f in flight_limit:		# this loop is too slow
	# 		delta = t_r - t_f
	# 		if delta >= 0 and delta < REINJ_DELAY_THRES:
	# 			# print 'reinj due to wnd limit:',t_r, delta
	# 			oppor_reinject.append(t_r)
	# 			break
	# 		if delta < 0:	# not found any flight_limit before this reinjection
	# 			break	# a tweak to make the script faster

	i=0
	t_f = flight_limit[i]
	
	for t_r in reinjection:
		# print t_r, datetime.fromtimestamp(t_r).strftime('%M:%S.%f')
		delta = t_r - t_f
		while :

			pass
		for t_f in flight_limit:		# this loop is too slow
			delta = t_r - t_f
			if delta >= 0 and delta < REINJ_DELAY_THRES:
				# print 'reinj due to wnd limit:',t_r, delta
				oppor_reinject.append(t_r)
				break
			if delta < 0:	# not found any flight_limit before this reinjection
				break	# a tweak to make the script faster



	print 'reinj due to wnd limit 	:',len(oppor_reinject)
	if args.verbose:
		for t in reinjection:
			if t not in oppor_reinject:
				print t, epoch2time(t)

	return (1, len(reinjection), len(oppor_reinject))

def epoch2time(t):
	return datetime.fromtimestamp(t).strftime('%M:%S.%f')

# from RichieHindle, stackoverflow
def immediate_subdir(a_dir):
    return [name for name in sorted(os.listdir(a_dir))	if os.path.isdir(os.path.join(a_dir, name))]

def main(dir):
	if (args.all):
		all_reinject   = 0
		oppor_reinject = 0

		connections = 0
		conn_all_reinj	= 0
		conn_opp_reinj	= 0

		for conn_dir in immediate_subdir(dir):
			(isConn, a, o) = analyseReinject(conn_dir)
			if isConn:
				connections +=1
			all_reinject 	+= a
			oppor_reinject 	+= o
			if a > 0:
				conn_all_reinj	+= 1
			if o > 0:
				conn_opp_reinj	+= 1

		print 'total reinjections:', all_reinject
		print 'oppor reinjections:', oppor_reinject
		print 'connections in total:', connections
		print 'connections w/ reinjections:', conn_all_reinj
		print 'connections w/ oppor reinj :', conn_opp_reinj
	else:
		analyseReinject(dir)

if __name__ == '__main__':
	dir = args.dir
	main(dir)

