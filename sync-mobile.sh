#!/bin/bash

cd ../../imc/real_smartphones/
rsync -e "ssh -i ~/.ssh/id_rsa"  --ignore-existing  -vhP hoang@nos:/home/mpcollect/collect/ns328523.ip-37-187-114.eu/real_smartphones/*.gz  .

# todo: if trace not exist -> unzip if did not -> copy trace
for zipfile in *.gz
do
	trace=${zipfile%.gz}
	echo $trace
	
	file=${trace}.pcap
	# copy trace from subfolder if it doesn't exist.
	if [ ! -f $file ]; then
		# if folder not exist, unzip
		gunzip -vk -c ${zipfile}  >  $file
	fi
done
