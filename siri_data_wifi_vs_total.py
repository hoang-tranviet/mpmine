#! /usr/bin/env python

#INPUT: 1)tcptrace long format contains tcp info: rtt-tcp.csv
	#	2)mptcptrace: subflow's tuple of each conn: stats_*.csv

#OUTPUT: data transfered over observed subflows (wifi) vs total mptcp data-acked
# it is interesting to see how siri prioritize wifi's paths.
	#   A major task is to link each subflow to a tcp connection.

import os
import re
from IPy import IP
import argparse

# column in sf_conn_{id}
STATE = 0
BYTE_C2S = 1
BYTE_S2C = 2
IPSRC = 3
IPDST = 4
SPORT =	5
DPORT =	6

ackPktC2S = 11

colID ={}
tcp_bytes_sent ={}
rttFile = "rtt-tcp.csv"

# load info of each TCP connection 
# from rtt-tcp.csv, which created by tcptrace
def load_TCP_info():
	for l in open(rttFile):
		if l[0]=='#':
			continue

		col = l.strip().split(',')
		if len(col) < 40:
			continue

		if col[0] == 'conn_#':
			field = col

			for id in range(len(field)):
				# print id, field[id]
				colID [field[id]] = id
			continue


		# #  this is IPv4-comp (e.g. ::91.109.28.55), tcptrace incorrectly print, 
		if re.match( '^:', col[1]):
			sIP = IP(":" + col[1])
		else: 
			sIP = IP(col[1])	# str to IP, using IPy.IP package


		dIP = IP(col[2])
		sport = col[3]
		dport = col[4]

		tuple = (sIP, dIP, sport, dport)		# beware that they are string!
		# print tuple

		# if col[1] =='148.251.69.238' and sport=='39088':
		# 	print tuple, rtt_avg, col[ackPktC2S], col[7:13], col[96], col[91:95]

		# rtt_avg: colID = 95 
		unique_bytes_sent_a2b =  int( col[colID['unique_bytes_sent_a2b'] ]) 
		unique_bytes_sent_b2a =  int( col[colID['unique_bytes_sent_b2a'] ]) 

		tcp_bytes_sent[tuple] = [unique_bytes_sent_a2b, unique_bytes_sent_b2a]


def get_tcp_bytes(sf):
	tuple = (IP(sf['sIP']), IP(sf['dIP']), sf['sport'], sf['dport'])
	if tuple in tcp_bytes_sent:
		return tcp_bytes_sent[tuple]
	else:
		# print  sf, 'not in tcptrace output, or RTT = 0'
		return 0

def process(f, of):

	wifi_data = 0
	sf = {}

	for l in open(f):
		col = l.strip().split(';')


		if col[2] == 'seqAcked':
			print 'MPTCP data:', col[3], '  ', col[4]

		elif col[2].startswith('sf_'):
			c = col[2].split('_')
			id = c[1]	# subflow id
			if c[2] == 'ip':
				if id not in sf:
					sf[id] = {}		# new subflow entry
				sf[id]['sIP'], sf[id]['dIP'] = col[3], col[4]
			elif c[2]=='port':
				sf[id]['sport'], sf[id]['dport'] = col[3], col[4]

	for id in sf:
		print 'TCP data: ', get_tcp_bytes(sf[id])
		print ''

def main(dir):
	of = open("out/siri-data-wifi-vs-total.txt", "w+")
	of.write("#conn_id  BytesSent	\n")

	load_TCP_info()
	print "Load TCP info done"

	for f in (os.listdir(dir)):
		if f.startswith('stats_'):
			# print f
			process(dir+f, of)


if __name__ == '__main__':
	parser = argparse.ArgumentParser( description = "data over wifi vs. MPTCP data acked")
	parser.add_argument('--dir', '-d', default= './')

	args = parser.parse_args()

	main(args.dir)