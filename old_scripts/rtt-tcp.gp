#!/usr/bin/gnuplot

# to plot rtt min,max,avg on the same graph

set term postscript eps enhanced color
set term postscript font 22

set output "rtt-all.eps"

#set size 0.9, 0.7

set xlabel "RTT of all subflows (ms)"
set ylabel "CDF"
set xrange [0.1:10000]

set logscale x
set format x "10^{%L}"

set key left top

set datafile missing '0'
# plot "rtt-tcp.csv" using  ($96):(1./280000)  smooth cumulative
plot "rtt-tcp.txt" using  ($1):(1./280644)  smooth cumulative  lw 2  title "min RTT",\
	 "rtt-tcp.txt" using  ($2):(1./280644)  smooth cumulative  lw 2  title "avg RTT" ,\
	 "rtt-tcp.txt" using  ($3):(1./280644)  smooth cumulative  lw 2  title "max RTT" ,\
	 "rtt-tcp.txt" using  ($4):(1./280644)  smooth cumulative  lw 2  title "maxRTT - minRTT"
