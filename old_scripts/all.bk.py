#! /usr/bin/env python

# master script

from subprocess import call
from datetime import *
from isoweek import Week
import time
import os
import argparse

duration = []

parser = argparse.ArgumentParser(description="Analyse all traces")
parser.add_argument('--dir', '-d', default="../../imc/mptcp167")  
								# captured from multipath-tcp.org
parser.add_argument('--firstweek', '-f', default='47')	#with .gz traces: from week 44
parser.add_argument('--lastweek', '-l',  default='70')	# range for imc: week 47 to 70

# '-n': do not run mptcptrace (if it was used before) 
parser.add_argument('--noparse', '-n', default= True, action='store_false', dest='parsing')
parser.add_argument('--extract', '-e', default= False, action='store_true')

args = parser.parse_args()


def main(dir):

	os.chdir(dir)

	cmd = "enjoy.py -d " + dir 
	if not args.parsing:
		cmd += ' -n '
	if args.extract:
		cmd += ' -e '

	# print cmd
	# for week in range(int(args.firstweek), int(args.lastweek) + 1):
	# 	print "week", week
	# 	call( cmd + " -w " + str(week), shell=True)


	os.chdir('week/')

	for week in os.listdir(os.getcwd()):
		if os.path.isdir(week):
			print week




# connection time
	# call("cat */conTime.txt > conTime.txt",shell=True)

	# for l in open('conTime.txt', 'r'):
	# 	duration.append(float(l))
	# print "number of connections: ", len(duration)

	rtts = loadRTT()
	plotRTT(rtts)

def loadRTT():

	rtts = []

	# call("cat */rtt-diff.txt > rtt-diff.txt",shell=True)

	CONN = 0
	BYTES =1
	RTTmin=2
	RTTmax=3
	RTTdiff=4

	for l in open("rtt-diff.txt",'r'):
		if l[0]=='#':
			continue

		col = l.strip().split('\t')
		if len(col) < 3:
			continue

		if int(col[BYTES]) >= 100*1024:	# only consider conn > 100KB
			rtts.append([float(col[RTTmin]), float(col[RTTmax]), float(col[RTTdiff])] )

	return rtts

def plotRTT(data):
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()

	x=[s[0] for s in data]
	x=np.sort(x)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='RTTmin', ls= '--', lw =1.5)

	x=[s[1] for s in data]
	x=np.sort(x)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='RTTmax', ls= '-', lw =1.5 )

	x=[s[2] for s in data]
	x=np.sort(x)
	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='RTTmax - RTTmin', ls= '-.', lw =1.5 )

	ax.set_xlim((1,10000))
	ax.set_xscale('log')

	ax.legend(loc='best')
	plt.savefig('rtt-100k.pdf')
	plt.show()


def plotduration():
	import numpy as np
	import matplotlib.pyplot as plt

	fig, ax = plt.subplots()

	x=np.sort(duration)

	cdf=np.arange(len(x))/float(len(x))
	ax.plot( x, cdf, label='connection duration (s)')

	ax.set_xscale('log')

	ax.legend()
	plt.savefig('conTime.pdf')
	plt.show()

if __name__ == '__main__':
	main(args.dir)