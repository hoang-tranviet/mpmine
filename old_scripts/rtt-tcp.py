#! /usr/bin/env python

# I dont know how to use gnuplot to plot logarithm CDF that contain zero value,
# this script also help excluding these zero value.


#INPUT:  tcptrace long format csv, with RTT option
#OUTPUT: RTT column (col 95 in tcptrace csv output)


ackPktC2S = 11

colID ={}
rtt ={}
RTT_avg=[]
f = "rtt-tcp.csv"
of = open("rtt-tcp.txt", "w+")

of.write('rtt_min \t rtt_avg  \t rtt_max  \t rtt(max-min) \n')	


for l in open(f):
	if l[0]=='#':
		continue

	col = l.strip().split(',')
	if len(col) < 40:
		continue

	if col[0] == 'conn_#':
		print len(col)
		field = col

		for id in range(len(field)):
			print id, field[id]
			colID [field[id]] = id
		continue

	if int(col[ackPktC2S]) < 3:
		continue

	sIP = col[1]
	dIP = col[2]
	sport = col[3]
	dport = col[4]

	tuple = (sIP, dIP, sport, dport)		# beware that they are string!
	# print tuple
	#rtt[tuple] = col[95] 
	rtt_avg =  float( col[colID['RTT_avg_b2a'] ]) 
	rtt_min =  float( col[colID['RTT_min_b2a'] ]) 
	rtt_max =  float( col[colID['RTT_max_b2a'] ]) 

	unique_bytes_sent = int(col[colID['unique_bytes_sent_b2a']])

	if (rtt_min > 0.2) and ( unique_bytes_sent > 100000):
		# there are some flows: 0 < rtt_avg < 0.2(ms): due to 
		RTT_avg.append(rtt_avg)
		of.write(str(rtt_min)+'\t'+ str(rtt_avg)+'\t'+ str(rtt_max)+'\t'+  str(rtt_max - rtt_min) + '\n')	

		if (rtt_min > 4) and (rtt_min < 8):
			print tuple
print len(RTT_avg)
#	rtt[tuple] = col[colID['RTT_avg_b2a']]
#	print rtt[tuple]
# a = np.sort(RTT_avg)
